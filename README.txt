ResumeScrape.py is the source from which resume_puller was written.

Original author: David Webster
Modifying author: Cyle DeLucca
Owning Company: iSenpai, LLC.

This program is designed to login to indeed.com and proceed to search a specified resumes that match a specified term.

Then it rips out all the words from the text of the page and proceeds to compare those words against a dictionary of know common words.  Once filtered it counts the words and prints them in a list.